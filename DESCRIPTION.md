This app packages Paperwork <upstream>dfa777944c7e</upstream>.

### About

Paperwork is an open source self hosted note-taking & archiving app -- an alternative to Evernote, Microsoft OneNote & Google Keep.

Paperwork allows you to create and manage notes to boost your productivity. Use it as a daily companion to keep track of your tasks or as a knowledge management system and archive. Share notes with others to collaborate.

This is the Cloudron port of Paperwork based on [Twostairs Paperwork](http://paperwork.rocks/).

### Contact

Website: [paperwork.rocks](http://paperwork.rocks/)

Github: [https://github.com/twostairs/paperwork](https://github.com/twostairs/paperwork)

Gitter: [https://gitter.im/twostairs/paperwork](https://gitter.im/twostairs/paperwork?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
