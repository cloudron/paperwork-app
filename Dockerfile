FROM cloudron/base:0.10.0

RUN mkdir -p /app/code
WORKDIR /app/code

ENV PATH /usr/local/node-4.7.3/bin:$PATH

RUN apt-get update && apt-get install -y php libapache2-mod-php php-mcrypt composer && rm -r /var/cache/apt /var/lib/apt/lists

# Version 1 from branch https://github.com/paperwork/paperwork/tree/1
RUN curl -L https://github.com/paperwork/paperwork/archive/dfa777944c7e3bc6ada57232b265a46d16ac105b.tar.gz | tar -xz --strip-components 1 -f -

# patch for https usage https://github.com/twostairs/paperwork/issues/281
# taken from https://github.com/JamborJan/paperwork/blob/master/frontend/app/routes.php#L14
RUN sed -i "13i\\URL::forceSchema('https');\\" /app/code/frontend/app/routes.php

# disable the admin panel which is not working as expected with LDAP (the admin panel only contains the user list)
RUN sed -i 's/isAdmin()/isAdmin() \&\& false/' /app/code/frontend/app/views/partials/navigation-main.blade.php

# There appears no other way to disable the bug reporting popup
RUN truncate -s 0 /app/code/frontend/app/views/partials/error-reporting-footer.blade.php

# build the app assets
RUN cd /app/code/frontend && \
    composer install && \
    npm install && \
    npm install gulp bower && \
    node_modules/.bin/bower --quiet --allow-root install && \
    node_modules/.bin/gulp

# Fix ldap_connect() to not have the ldap:// prefix. Not sure why this is required
RUN sed -e 's,ldap://,,' -i /app/code/frontend/vendor/strebl/adldap/lib/adLDAP/adLDAP.php

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/paperwork.conf /etc/apache2/sites-enabled/paperwork.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN a2enmod rewrite

# configure php
RUN crudini --set /etc/php/7.0/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.save_path /run/paperwork/sessions && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_divisor 100

RUN ln -sf /run/paperwork/database.php /app/code/frontend/app/config/database.php
RUN ln -sf /run/paperwork/ldap.php /app/code/frontend/app/config/ldap.php
RUN rm -rf /app/code/frontend/app/storage && ln -s /app/data /app/code/frontend/app/storage

COPY auth.php.template /app/code/frontend/app/config/auth.php
COPY start.sh database.php.template ldap.php.template paperwork.json.template /app/code/

RUN chown -R www-data.www-data /app/code

CMD [ "/app/code/start.sh" ]
