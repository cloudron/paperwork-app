# Paperwork Cloudron App

This repository contains the Cloudron app package source for [Paperwork](https://github.com/twostairs/paperwork).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=rocks.paperwork.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id rocks.paperwork.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd paperwork-app
cloudron build
cloudron install
```

## Debugging

Paperwork uses the laravel framework. Errors are reported with a 500 response and a trace ui in the browser.
General logging happens to frontend/app/storage/logs/laravel.log

Use this in the php:
use \Log;
Log::info("blah");

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the notes are still ok.

```
cd gogs-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

