#!/bin/bash

set -eu

mkdir -p /app/data/attachments /app/data/cache /app/data/logs /app/data/meta /app/data/sessions /app/data/views /app/data/config /run/paperwork/sessions
touch /app/data/db_settings

sed -e "s/##MYSQL_HOST/${MYSQL_HOST}/" \
    -e "s/##MYSQL_PORT/${MYSQL_PORT}/" \
    -e "s/##MYSQL_DATABASE/${MYSQL_DATABASE}/" \
    -e "s/##MYSQL_USERNAME/${MYSQL_USERNAME}/" \
    -e "s/##MYSQL_PASSWORD/${MYSQL_PASSWORD}/" \
    -e "s/##REDIS_HOST/${REDIS_HOST}/" \
    -e "s/##REDIS_PORT/${REDIS_PORT}/" \
    /app/code/database.php.template > /run/paperwork/database.php

sed -e "s/##LDAP_PORT/${LDAP_PORT}/" \
    -e "s/##LDAP_SERVER/${LDAP_SERVER}/" \
    -e "s/##LDAP_USERS_BASE_DN/${LDAP_USERS_BASE_DN}/" \
    -e "s/##LDAP_BIND_DN/${LDAP_BIND_DN}/" \
    -e "s/##LDAP_BIND_PASSWORD/${LDAP_BIND_PASSWORD}/" \
    /app/code/ldap.php.template > /run/paperwork/ldap.php

# skip the setup wizard (https://github.com/twostairs/paperwork/issues/649)
cp /app/code/paperwork.json.template /app/data/config/paperwork.json
echo "{}" > /app/data/config/database.json      # we use a custom database.php because redis settings are not part database.json yet
echo "8" > /app/data/config/setup

# make paperwork own its own data
chown -R www-data.www-data /app/data /run/paperwork/

cd /app/code/frontend
php artisan migrate --no-interaction --force

echo "Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
